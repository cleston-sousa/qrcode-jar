package x3sc;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class MainApp {

  public static int textCeil = 10;
  public static int textTopMargin = 5;
  public static List<String> supportedFormats = Arrays.asList("jpg", "png", "gif");

  public static void argumentError(String arg) {
    throw new IllegalArgumentException("Not a valid argument: " + arg);
  }

  public static void main(String[] args) throws Exception {
    QrCode qrc = new QrCode();

    // args = "-path=myImage -qrcode=simple underline messages".split(" ");

    for (int i = 0; i < args.length; i++) {
      String arg = args[i];
      if (qrc.text.size() > 0) {
        qrc.text.add(arg);
        continue;
      }
      switch (arg.charAt(0)) {
      case '-':
        if (arg.length() == 1)
          argumentError(arg);
        try {

          String fieldName = args[i].substring(1);
          String value = "";
          if (fieldName.indexOf("=") > 0) {
            int eqs = fieldName.indexOf("=");
            fieldName = fieldName.substring(0, eqs);
            value = args[i].substring(eqs + 2);
          }

          Field f = qrc.getClass().getDeclaredField(fieldName);
          f.setAccessible(true);
          Class<?> t = f.getType();
          if (t.equals(String.class)) {
            f.set(qrc, value);
          } else if (t.equals(boolean.class)) {
            f.set(qrc, true);
          } else if (t.equals(int.class)) {
            int intVal = Integer.parseInt(value);
            f.set(qrc, intVal);
          } else {
            if (t.equals(Color.class)) {
              Field fc = Color.class.getDeclaredField((value).toUpperCase());
              f.set(qrc, fc.get(null));
            } else if (t.equals(List.class)) {
              qrc.text.add(value);
            }
          }
        } catch (Exception e) {
          argumentError(arg);
        }
        break;
      default:
        qrc.text.add(arg);
      }
    }

    if (qrc.format == null || supportedFormats.indexOf(qrc.format.toLowerCase()) < 0)
      argumentError("-format");

    if (qrc.qrcode == null || "".equals(qrc.qrcode))
      argumentError("-qrcode");

    qrc.format = qrc.format.toLowerCase();

    createImage(qrc);
  }

  public static void createImage(QrCode qrc) throws Exception {
    // define image size
    Size imgSize = new Size(qrc.width, qrc.height);
    for (String txt : qrc.text) {
      Size txtSize = textSize(txt, qrc);
      imgSize.height = imgSize.height + (textTopMargin + txtSize.height);
      imgSize.width = imgSize.width > txtSize.width ? imgSize.width : txtSize.width;
    }

    // qrcode
    Hashtable<EncodeHintType, ErrorCorrectionLevel> hm = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
    hm.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
    QRCodeWriter qrCodeWriter = new QRCodeWriter();
    BitMatrix bm = qrCodeWriter.encode(qrc.qrcode, BarcodeFormat.QR_CODE, qrc.width, qrc.height, hm);

    // image
    BufferedImage bi = new BufferedImage(imgSize.width, imgSize.height, BufferedImage.TYPE_INT_RGB);
    bi.createGraphics();
    Graphics2D g2d = (Graphics2D) bi.getGraphics();
    g2d.setColor(Color.WHITE);
    g2d.fillRect(0, 0, imgSize.width, imgSize.height);

    // draw qrcode
    g2d.setColor(qrc.color);
    int centeredX = (imgSize.width - qrc.width) / 2;
    int centeredY = 0;
    for (int i = 0; i < qrc.width; i++) {
      for (int j = 0; j < qrc.height; j++) {
        if (bm.get(i, j)) {
          g2d.fillRect(i + centeredX, j, 1, 1);
        }
      }
    }

    Font textFont = new Font(qrc.fontName, qrc.fontStyle, qrc.fontSize);

    // draw text
    int cursorY = qrc.height;
    if (qrc.text.size() > 0) {
      g2d.setFont(textFont);
    }
    for (String txt : qrc.text) {
      Size txtSize = textSize(txt, qrc);
      cursorY = cursorY + textTopMargin;
      centeredX = (imgSize.width - txtSize.width) / 2;
      centeredY = cursorY + txtSize.ascent;
      g2d.drawString(txt, centeredX, centeredY);
      cursorY = cursorY + txtSize.height;
    }

    // write file
    String fileName = FilenameUtils.getName(qrc.path);
    String filePath = FilenameUtils.getFullPath(qrc.path);
    filePath = filePath == null ? "" : filePath;
    if ("".equals(fileName) || fileName == null) {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
      fileName = sdf.format(new Date());
    }
    File fileToGen = new File(filePath + fileName + "." + qrc.format);
    if (fileToGen.getParentFile() != null) {
      fileToGen.getParentFile().mkdirs();
    }
    fileToGen.createNewFile();
    ImageIO.write(bi, qrc.format, fileToGen);
  }

  public static Size textSize(String text, QrCode qrc) {
    BufferedImage img = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2d = img.createGraphics();
    g2d.setFont(new Font(qrc.fontName, qrc.fontStyle, qrc.fontSize));
    FontMetrics fm = g2d.getFontMetrics();
    Rectangle2D r2d = fm.getStringBounds(text, g2d);
    g2d.dispose();
    double w = r2d.getWidth();
    double h = r2d.getHeight();
    Size s = new Size(((int) Math.ceil(Math.abs(w) / textCeil) * textCeil),
        ((int) Math.ceil(Math.abs(h) / textCeil) * textCeil), fm.getAscent());
    return s;
  }

}

class Size {
  int height;
  int width;
  int ascent = 0;

  public Size(int width, int height) {
    super();
    this.height = height;
    this.width = width;
  }

  public Size(int width, int height, int ascent) {
    super();
    this.height = height;
    this.width = width;
    this.ascent = ascent;
  }
}

class QrCode {
  String qrcode;
  String path;
  String format = "png";
  int height = 115;
  int width = 115;
  List<String> text = new ArrayList<String>();
  Color color = Color.BLACK;
  int fontSize = 22;
  String fontName = Font.MONOSPACED;
  int fontStyle = Font.BOLD;
}
