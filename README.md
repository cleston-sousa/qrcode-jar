# README #

Simple JAR application for generate a qrcode jpg file

**java -jar qrcode-0.6.jar -qrcode=content** [-opcoes=valor] [texto ... n]

OPCOES : DESCRICAO

**-qrcode**: Conteudo que sera convertido em QR-Code
**[content]** obrigatorio

**-path**: Nome do aquivo com ou sem caminho de diretorio utilizar barras **'/'** para a separacao de diretorios
[nomeDoArquivo] padrao "yyyyMMddHHmmss"

**-format**: Formato da image, suporta somente [PNG, JPG, GIF]
[type] padrao PNG

**-height**: Altura do codigo em pixels
[num] padrao 115

**-width**: Largura do codigo em pixels
[num] padrao 115

**-color **: Cores definidas em java.awt.Color. Exemplos: BLACK, BLUE, RED, GRAY
[colorName] padrao BLACK

**-fontSize**: Tamanho da fonte do texto
[tamanho] padrao 22

**-fontName**: Nome da fonte que sera utilizada para o texto
[nome] Font.MONOSPACED

**-fontStyle**: (em adaptacao)


**-text**: Cada argumento depois desta chave escrevera uma linha abaixo do QR-Code gerado
Todo argumento nao identificado como opcao sera passado como texto


